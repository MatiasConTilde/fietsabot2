import { useState, useEffect } from 'react'
import { Box, Form, Icon, Table } from 'react-bulma-components'
import { Search as SearchIcon } from 'react-ionicons'
import request from '../helpers/request'
import Loading from '../components/Loading'
import SearchSong from '../components/SearchSong'

const Search = () => {
  const [query, setQuery] = useState('')
  const [songs, setSongs] = useState([])
  const [state, setState] = useState('')

  useEffect(() => {
    if (query.length > 0) {
      return request(
        'search',
        { query },
        setState,
        (songs) => setSongs(songs)
      )
    } else {
      setSongs([])
      setState('ok')
    }
  }, [query])

  const play = song => {
    setQuery('')

    return request(
      'play',
      { url: song.url },
      setState,
      (song) => console.log(song)
    )
  }

  return (
    <Box>
      <Form.Field>
        <Form.Control>
          <Form.Input placeholder='Search...' name='name' value={query} onChange={event => setQuery(event.target.value)} />
          <Icon align='left'>
            <SearchIcon />
          </Icon>
        </Form.Control>
      </Form.Field>

      { state === 'loading' ?
          <Loading/> :

          songs.length > 0 ?
            <Table hoverable={true} size={'fullwidth'} >
              <tbody>
                {songs.map(s => <SearchSong song={s} onClick={() => play(s)} key={s.isrc} />)}
              </tbody>
            </Table> :

            <></> }
    </Box>
  )
}

export default Search
