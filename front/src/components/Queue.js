import { useState, useEffect } from 'react'
import { Box, Table } from 'react-bulma-components'
import request from '../helpers/request'
import QueueSong from '../components/QueueSong'

const Queue = () => {
  const [queue, setQueue] = useState([])

  useEffect(() => {
      return request(
        'queue',
        {},
        () => {},
        (q) => setQueue(q)
      )
  }, [])

  const send_vote = (song) => {
    return request(
      'vote',
      { id: song.id, user_id: localStorage.getItem('user_id') },
      () => {},
      (q) => setQueue(q)
    )
  }

  if (queue.length > 0) {
    return (
      <Box>
        <Table hoverable={true} size={'fullwidth'} >
          <tbody>
            {queue.map(s => <QueueSong song={s} onClick={() => send_vote(s)} key={s.isrc} />)}
          </tbody>
        </Table>
      </Box>
    )
  } else {
    return (
      <Box>
        <p>No songs in the queue</p>
      </Box>
    )
  }
}

export default Queue
