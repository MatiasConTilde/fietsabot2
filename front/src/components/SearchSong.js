import { Image } from 'react-bulma-components'

const SearchSong = ({ song, onClick }) => {
  return (
    <tr onClick={onClick} >
      <td><Image src={song.cover_url} size={96} /></td>
      <td>{song.title}</td>
      <td>{song.artist}</td>
      <td>{song.album}</td>
    </tr>
  )
}

export default SearchSong
