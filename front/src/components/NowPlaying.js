import { useState, useEffect } from 'react'
import { Box, Table, Image } from 'react-bulma-components'
import request from '../helpers/request'

const NowPlaying = () => {
  const [song, setSong] = useState({})

  useEffect(() => {
    return request(
      'playing',
      {},
      () => {},
      (s) => setSong(s)
    )
  }, [])

  if (song.title) {
    return (
      <Box>
        <Table size={'fullwidth'} >
          <tbody>
            <td><Image src={`${process.env.REACT_APP_API_URL}/cover/${song.isrc}`} size={96} /></td>
            <td>{song.title}</td>
            <td>{song.artist}</td>
            <td>{song.album}</td>
          </tbody>
        </Table>
      </Box>
    )
  } else {
    return (
      <Box>
        <p>Nothing playing</p>
      </Box>
    )
  }
}

export default NowPlaying
