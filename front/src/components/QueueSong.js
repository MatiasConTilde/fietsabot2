import { Button, Image, Icon } from 'react-bulma-components'
import { ThumbsUpSharp } from 'react-ionicons'

const QueueSong = ({ song, onClick }) => {
  const voted = song.votes?.includes(localStorage.getItem('user_id'))

  return (
    <tr onClick={onClick} >
      <td><Image src={`${process.env.REACT_APP_API_URL}/cover/${song.isrc}`} size={96} /></td>
      <td>{song.title}</td>
      <td>{song.artist}</td>
      <td>{song.album}</td>
      <td>
        <Button color={voted ? 'success' : ''}>
          <p>{song.votes?.length}</p>
          <Icon>
            <ThumbsUpSharp color={voted ? 'white' : ''}/>
          </Icon>
        </Button>
      </td>
    </tr>
  )
}

export default QueueSong
