import { Box, Columns, Progress } from 'react-bulma-components'

const Loading = () => {
  return (
    <Box>
      <Columns>
        <Columns.Column narrow={true} className='has-text-centered'>
          Loading...
        </Columns.Column>
        <Columns.Column>
          <Progress color='info' />
        </Columns.Column>
      </Columns>
    </Box>
  )
}

export default Loading
