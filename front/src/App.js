import { Section, Block, Columns } from 'react-bulma-components'
import Search from './components/Search'
import NowPlaying from './components/NowPlaying'
import Queue from './components/Queue'

const App = () => {
  if (!localStorage.getItem('user_id')) {
    localStorage.setItem('user_id', Math.random().toString(36).slice(2, 18))
  }

  return (
    <>
    <Section>
      <Block>
        <Columns className='is-centered'>
          <Columns.Column className='is-three-quarters-desktop is-two-thirds-fullhd'>
            <Search />
          </Columns.Column>
        </Columns>
      </Block>

      <Block>
        <Columns className='is-centered'>
          <Columns.Column className='is-three-quarters-desktop is-two-thirds-fullhd'>
            <NowPlaying />
          </Columns.Column>
        </Columns>
      </Block>

      <Block>
        <Columns className='is-centered'>
          <Columns.Column className='is-three-quarters-desktop is-two-thirds-fullhd'>
            <Queue />
          </Columns.Column>
        </Columns>
      </Block>
    </Section>

    <Section />
    <Section />

    </>
  )
}

export default App
