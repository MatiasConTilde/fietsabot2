# Fietsabot2 frontend

App para gestionar música democráticamente

## Run

You need pulseaudio on your computer

```bash
cd back
docker compose up -d
```

```bash
cd front
npm install
REACT_APP_API_URL='http://localhost:8000' npm start
```
