import os
from fastapi import FastAPI, Response
from spotdl import Spotdl
from spotdl.utils.search import get_search_results
from spotdl.types.song import Song
from mpd import MPDClient
from mpd.base import CommandError
from collections import defaultdict
import time

import nest_asyncio
nest_asyncio.apply()

app = FastAPI()
spotdl = Spotdl(client_id='5f573c9620494bae87890c0f08a60293',
                client_secret='212476d9b0f3472eaa762d90b19b0ba8',
                audio_providers=['youtube-music'],
                output_format='m4a',
                output='./music/{isrc}')

class MPD(object):
    def __init__(self):
        self.host = os.environ['MPD_HOST']
        self.port = 6600

    def __enter__(self):
        self.mpdclient = MPDClient()
        self.mpdclient.connect(self.host, self.port)
        return self.mpdclient

    def __exit__(self, *args):
        self.mpdclient.disconnect()

votes = defaultdict(list)

def get_song(s):
    return {
        'isrc': s['file'].split('.')[0],
        'title': s['title'],
        'artist': s['artist'],
        'album': s['album'],
        'id': int(s['id']),
        'pos': int(s['pos']),
        'votes': votes[int(s['id'])],
    }

def get_song_spotify(s):
    return {
        'isrc': s.isrc,
        'title': s.name,
        'artist': s.artist,
        'album': s.album_name,
        'url': s.url,
        'cover_url': s.cover_url
    }

def get_queue():
    with MPD() as mpd:
        current_song = mpd.currentsong()
        current_pos = int(current_song['pos'])+1 if 'pos' in current_song else 0
        queue = mpd.playlistinfo()[current_pos:]
    return [get_song(s) for s in queue]

@app.get('/search')
def search(response: Response, query: str = None):
    songs = get_search_results(query)
    response.headers['Cache-Control'] = 'max-age=3600'
    return [get_song_spotify(s) for s in songs]

@app.get('/play')
def play(url: str = None):
    song = Song.from_url(url)

    for s in get_queue():
        if song.isrc == s['isrc']:
            return get_song_spotify(song)

    spotdl.download(song)

    file = f'{song.isrc}.m4a'

    with MPD() as mpd:
        mpd.update(file)
        while True:
            try:
                mpd.add(file)
                break
            except CommandError:
                time.sleep(0.1)
                pass

        if mpd.status()['state'] == 'stop':
            mpd.play(0)

    return get_song_spotify(song)

@app.get('/playing')
def playing():
    with MPD() as mpd:
        song = mpd.currentsong()
    if 'file' in song:
        return get_song(song)
    else:
        return {}

@app.get('/queue')
def queue():
    return get_queue()

@app.get('/vote')
def vote(id: int, user_id: str):
    queue = get_queue()

    if not user_id in votes[id]:
        votes[id].append(user_id)
        for s in queue:
            if len(s['votes']) < len(votes[id]) or s['id'] == id:
                with MPD() as mpd:
                    mpd.moveid(id, s['pos'])
                break
    else:
        votes[id].remove(user_id)
        for s in queue[::-1]:
            if len(s['votes']) > len(votes[id]) or s['id'] == id:
                with MPD() as mpd:
                    mpd.moveid(id, s['pos'])
                break

    return get_queue()

@app.get('/cover/{isrc}')
def cover(isrc):
    with MPD() as mpd:
        data = mpd.readpicture(f'{isrc}.m4a')
    return Response(content=data['binary'], media_type=data['type'], headers={'Cache-Control': 'max-age=2592000'})
